Release notes for dwarf radar v.1 (until I think of a better name)
==================================================

11.4.2018 release

About
----------------

Dwarf radar is a hacky script I threw together based on other ppls and my code. It started as a proof of concept because I read in an old thread that someone wanted it, and I wanted it and someone said you couldn't do it.  It turns out you can.

The radar will display a scene from another part of the current game map.  This can show things not displayed on the main screen including on different z-levels.  Please note the screen is in no way complete.  The main thing I wanted to show was some of the critters, walls, and some other main structures like stairs, etc. 

The radars basic operation is to show a single scene based on the info submitted to it.  However, I also included a script that I use to follow a dwarf or critter around.  

The radar is pretty stable but might be a memory or cpu hog.  I haven't had any issues with it, but I haven't really tested its performance.


Package info
----------------

This package includes several files that can be used to run the dwarf radar.  The list of files included follows:

bmods/radar.exe - This is a windows version of the interface that listens on network port for incoming radar information and displays data

bmods/radar.py - This provides an interface that listens on network port for incoming radar information and displays the data

hack/scripts/radar.lua - This script is the main file to run the radar. It queries the game to try to determine the location of a unit based on a unit id passed into it.

hack/scripts/radarfollow.lua - This script will send a repeat command to radar.lua with the id of the unit to show, essentially following it around

hack/scripts/radarcurrentlocation.lua - This script will stop the radar on the current coordinates and track whatever is in that location


hack/scripts/showdwarves.lua - This script will print to the dfhack screen the names and ids of all the dwarves in the fort

hack/scripts/showcritters.lua - This script will print to the dfhack screen the names and ids of all the critters in the current game (not world)


Requirements
---------------

Dfhack 0.44.12-r1 (release) on x86_64 (last tested on)
Dwarf fortress -.44.12

Windows
-------
Just run radar.exe

Python
-------
Python 2.7
PyQt4 (windows wheels included)


Note: Versions should all be the same. If using 64bit df - you might want to use 64 bit python, etc.

Installation Windows 
---------------

Install dwarf fortress.

Install dfhack

Unzip file in your dwarf fortress root directory.


Installation Python
---------------

Install dwarf fortress.

Install dfhack

Unzip file in your dwarf fortress root directory.

Install python 2.7 

From a console navigate to bmods folder under installation directory

If using 32bit type:

pip install PyQt4-4.11.4-cp27-cp27m-win32.whl

Else if using 64bit type:

pip install PyQt4-4.11.4-cp27-cp27m-win_amd64

Congrats you have installed everything.


Usage
--------------

The scripts only work in game. 

In the dfhack console you can type:

showdwarves 

This shows all dwarves in fort, profession and id

In the dfhack console you can type:

showcritters

This shows all critters around fort, their species and id.

If using python to activate the radar in a command console navigate to the bmods directory and type:

python radar.py

If using windows just double click:

radar.exe

A blank window will display. 

To follow a dwarf/critter with the radar in the dfhack console type:
radarfollow UNIT unit_id

Where unit_id is the unit id of the unit you want to follow. For example:
radarfollow UNIT 4517

To follow a Location with the radar in the dfhack console type:
radarfollow LOCATION x y z

A screen showing the unit and its surroundings should show

To monitor the current radar location in the dfhack console type:

radarcurrentlocation

Other notes
-----------------

Yes I realize the code isn't great.  This is my first df project and first with lua and python.  Not that my code will get any better, this is just done for fun.

The code can be buggy too.  I've only tested it on my stream.  

There is a lot of stuff missing and some stuff might be coded with wrong characters, etc.  

However - there is a lot of potential. It could possibly serve as a window for a multiplayer feature or monitoring those pesky assassin dwarves.  Maybe it could eventually show graphic content or be hacked for adventure mode.  idk - lots of possibilites if it doesn't break more stuff.

glhf - and thanks to everyone whose code I stole - creators of everything df that have given me so much joy in my life :D

If you want to help develop this further feel free to contact me or visit my stream - https://www.twitch.tv/seyedaed

