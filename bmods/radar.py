# -*- coding: utf-8 -*-
import sys
import os
import json

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4 import QtCore, QtGui, QtNetwork
import SocketServer
import threading


DEFAULT_UNIV_X = 19
DEFAULT_UNIV_Y = 19
CP437 = [u"NULL",u"☺",u"☻",u"♥",u"♦",u"♣",u"♠",u"•",u"◘",u"○",u"◙",u"♂",u"♀",u"♪",u"♫",
u"☼",u"►",u"◄",u"↕",u"‼",u"¶",u"§",u"▬",u"↨",u"↑",u"↓",u"→",u"←",u"∟",u"↔",u"▲",u"▼",u""
,u"!",u"\"",u"#",u"$",u"%",u"&",u"'",u"(",u")",u"*",u"+",u""",u""",u"-",u".",u"/",u"0",
u"1",u"2",u"3",u"4",u"5",u"6",u"7",u"8",u"9",u":",u";",u"<",u"=",u">",u"?",u"@",u"A",u"B",
u"C",u"D",u"E",u"F",u"G",u"H",u"I",u"J",u"K",u"L",u"M",u"N",u"O",u"P",u"Q",u"R",u"S",u"T",
u"U",u"V",u"W",u"X",u"Y",u"Z",u"[",u"\\",u"]",u"^",u"_",u"`",u"a",u"b",u"c",u"d",u"e",u"f",
u"g",u"h",u"i",u"j",u"k",u"l",u"m",u"n",u"o",u"p",u"q",u"r",u"s",u"t",u"u",u"v",u"w",u"x",
u"y",u"z",u"{",u"|",u"}",u"~",u"⌂",u"Ç",u"ü",u"é",u"â",u"ä",u"à",u"å",u"ç",u"ê",u"ë",u"è",
u"ï",u"î",u"ì",u"Ä",u"Å",u"É",u"æ",u"Æ",u"ô",u"ö",u"ò",u"û",u"ù",u"ÿ",u"Ö",u"Ü",u"¢",u"£",
u"¥",u"₧",u"ƒ",u"á",u"í",u"ó",u"ú",u"ñ",u"Ñ",u"ª",u"º",u"¿",u"⌐",u"¬",u"½",u"¼",u"¡",u"«",
u"»",u"░",u"▒",u"▓",u"│",u"┤",u"╡",u"╢",u"╖",u"╕",u"╣",u"║",u"╗",u"╝",u"╜",u"╛",u"┐",u"└",
u"┴",u"┬",u"├",u"─",u"┼",u"╞",u"╟",u"╚",u"╔",u"╩",u"╦",u"╠",u"═",u"╬",u"╧",u"╨",u"╤",u"╥",
u"╙",u"╘",u"╒",u"╓",u"╫",u"╪",u"┘",u"┌",u"█",u"▄",u"▌",u"▐",u"▀",u"α",u"ß",u"Γ",u"π",u"Σ",
u"σ",u"µ",u"τ",u"Φ",u"Θ",u"Ω",u"δ",u"∞",u"φ",u"ε",u"∩",u"≡",u"±",u"≥",u"≤",u"⌠",u"⌡",u"÷",
u"≈",u"°",u"∙",u"·",u"√",u"ⁿ",u"²",u"■", u" "]

COLORS = [
    #alpha 0 colors
    [ Qt.black, Qt.darkBlue, Qt.darkGreen, Qt.darkCyan,
      Qt.darkRed, Qt.darkMagenta, QColor(217,118,65), Qt.darkGray
    ],
    #alph 1 colors
    [ Qt.gray, Qt.blue, Qt.green, Qt.cyan, 
      Qt.red, Qt.magenta, Qt.yellow, Qt.white
    ]

]


class dfradar(QGraphicsView):
    """The Qt Graphics Grid where the whole action takes place.
    """

    def __init__(self):
        QGraphicsView.__init__(self)
        self.scene = QGraphicsScene()
        self.setScene(self.scene)
        self.drawUniverse(DEFAULT_UNIV_X, DEFAULT_UNIV_Y)
        #self.timer = QTimer()
        #self.timer.timeout.connect(self.rePaint)

    def startAnimation(self, universe):
        """Start the animation timer.
        """
        self.universe = universe
        #self.timer.start(100)

    def stopAnimation(self):
        """Stop the animation timer.
        """
        #print("stop")
        #self.timer.stop()

    def rePaint(self):
        """Animate the cell generations.
        """
        self.clearScene()
        self.drawUniverse(DEFAULT_UNIV_X, DEFAULT_UNIV_Y)


    def clearScene(self):
        """Clear the scene.
        """
        self.scene.clear()

    def drawUniverse(self, rows, columns):
        """Draw the universe grid.
        """
        self.scene.setSceneRect(QRectF(0, 0, rows, columns))
        # draw vertical lines
        for x in xrange(rows):
            self.scene.addLine(x, 0, x, columns, QPen(Qt.black))

        # draw horrizontal lines
        for y in xrange(columns):
            self.scene.addLine(0, y, rows, y, QPen(Qt.black))

        self.fitInView(self.scene.itemsBoundingRect())

    def drawCellAt(self, x, y):
        """Fill the cell at grid location (x, y)
        """
        item = QGraphicsRectItem(x, y, 1, 1)
        item.setBrush(QBrush(Qt.black))
        self.scene.addItem(item)

    def drawCellAtWithText(self, x, y, data):
        """Fill the cell at grid location (x, y)
        """
        item = QGraphicsRectItem(x, y, 1, 1)
        font = QtGui.QFont("Arial",1)
        text = QGraphicsSimpleTextItem(parent=item)
        text.setFont(font)
        textToShow = QString(data[0])
        textToShow.resize(1)

        text.setText(textToShow)
        text.setPos(x +.1,y-1.2)
        text.setBrush(QBrush(Qt.black))

        # Center text according to rectItem size

        item.setBrush(QBrush(Qt.white))
        self.scene.addItem(item)
        self.scene.addItem(text)

    def drawCellAtWithTile(self, x, y, data):
        """Fill the cell at grid location (x, y)
        """
        item = QGraphicsRectItem(x, y, 1, 1)
        font = QtGui.QFont("Arial",1)
    
        text = QGraphicsSimpleTextItem(parent=item)
        text.setFont(font)  
        #print(data[0])
        item2 = CP437[int(data)]

        textToShow = QString(item2)
        textToShow.resize(1)

        text.setText(textToShow)
        text.setPos(x+.1,y-1.2)
        text.setBrush(QBrush(Qt.white))

        # Center text according to rectItem size

        self.scene.addItem(item)
        #self.scene.addItem(text)

    def drawCellAtWithTileColor(self, x, y, data, fgcolor, bgcolor, alpha):
        """Fill the cell at grid location (x, y)
        """
        #print(fgcolor + " " + bgcolor + " " + alpha)

        item = QGraphicsRectItem(x, y, 1, 1)
        font = QtGui.QFont("Arial",1)
        item.setBrush(QBrush(COLORS[0][int(bgcolor)]))

        text = QGraphicsSimpleTextItem(parent=item)
        text.setFont(font)
        #print(data[0])
        item2 = CP437[int(data)]

        textToShow = QString(item2)
        textToShow.resize(1)

        text.setText(textToShow)
        text.setPos(x+.1,y-1.2)
        #print(str(alpha) + " ---  "  + str(fgcolor))
        text.setBrush(QBrush(COLORS[int(alpha)][int(fgcolor)]))

        # Center text according to rectItem size

        self.scene.addItem(item)
        #self.scene.addItem(text)

class DwarfView(QDialog):

    def __init__(self, parent=None):
        super(DwarfView, self).__init__(parent)
        # Qt Graphics view where the whole action happens
        self.universeView = dfradar()
        self.setStyleSheet("background-color: 'rgb(1, 0, 0);'")
        self.setWindowTitle("DwarfRadar")

        # set the window layout
        layout = QVBoxLayout()


        self.msglabel = QLabel('Hello, me!')
        self.msglabel.setStyleSheet('color: white')
        labelFont2 = QtGui.QFont("Times", 12, QtGui.QFont.Bold) 
        self.msglabel.setFont(labelFont2)

        layout.addWidget(self.msglabel)
        layout.addWidget(self.universeView)



        #selectionArea = QGroupBox('Select a Pattern')
        #selectionLayout = QHBoxLayout()
        #selectionArea.setLayout(selectionLayout)
        self.requestdata = ""
        self.setLayout(layout)

        self.tcpServer = QtNetwork.QTcpServer(self)
        if not self.tcpServer.listen(port=9999):
            #print("a weird error occurred")
            self.close()

        self.tcpServer.newConnection.connect(self.handleData)

    def handleData(self):

        clientConnection = self.tcpServer.nextPendingConnection()
        if( clientConnection.peerAddress().toString () != '127.0.0.1'):
            print("connection from foreign host:  "+ clientConnection.peerAddress().toString ())
            return
    
        clientConnection.readyRead.connect(self.process_request)
        clientConnection.disconnected.connect(clientConnection.deleteLater)

    def process_request(self):
        socket = self.sender()
        requestdata = socket.readAll()
        requestdata = requestdata.replace('\r\n','')

        #print(str(requestdata))
        self.loadText(str(requestdata))

    def loadText(self, data):
        self.universeView.rePaint()
        #print("starting new--------------")
        #if(self.requestdata != ""):
            #print("continuing")
        #with open('radarinfo.txt', 'r') as myfile:
            #data=myfile.read().replace('\n', '')
        self.requestdata = self.requestdata + data
        try:

            datastore = json.loads(self.requestdata.decode('utf-8','ignore'))
            self.requestdata = ""
        except Exception as e:
            print("toolong " + str(e))
            return
        #print(datastore)
        #print(datastore["creatures"][0]["name"])
        #print(datastore["creatures"][0]["xcoord"])
        #print(datastore["creatures"][0]["ycoord"])

        self.setCenter(datastore["creatures"][0]["xcoord"], datastore["creatures"][0]["ycoord"])

        self.msglabel.setText("Tracking location: z="+str(datastore["creatures"][0]["zcoord"]) 
                                + " x=" +str(datastore["creatures"][0]["xcoord"]) 
                                + " y=" +str(datastore["creatures"][0]["ycoord"])
                                + " following " + str(datastore["creatures"][0]["name"])
                                + " the " +  str(datastore["creatures"][0]["descript"])
                                )



        for tiles in datastore["tiles"][0:]:

            #print(tiles)
            #print(str(tiles["xcoord"]) + " " + str(tiles["ycoord"]))


            dx, dy = self.getMapCoordinates(tiles["xcoord"], tiles["ycoord"])

            if tiles["hascolor"] == "true":
                self.universeView.drawCellAtWithTileColor(dx, dy, tiles["tile"], tiles["fgcolor"], tiles["bgcolor"], tiles["alpha"])
            else:
                self.universeView.drawCellAtWithTile(dx, dy, tiles["tile"])


        dx, dy = self.getMapCoordinates(datastore["creatures"][0]["xcoord"], datastore["creatures"][0]["ycoord"])
        #self.universeView.drawCellAtWithText(dx, dy, datastore["creatures"][0]["descript"])
        #self.universeView.drawCellAt(dx, dy)


        self.universeView.drawCellAtWithTile(dx, dy, datastore["creatures"][0]["tile"])
        #print("before fore")
        for items in datastore["creatures"][0:]:
            #print(items["name"])
            #print(str(items["ycoord"]) + " " + str(items["xcoord"]))
            dx, dy = self.getMapCoordinates(items["xcoord"], items["ycoord"])
            #self.universeView.drawCellAtWithText(dx, dy, items["descript"])
            #self.universeView.drawCellAt(dx, dy)
            if items["hascolor"] == "true":
                self.universeView.drawCellAtWithTileColor(dx, dy, items["tile"], items["fgcolor"], items["bgcolor"], items["alpha"])
            else:
                self.universeView.drawCellAtWithTile(dx, dy, items["tile"])


            #self.universeView.drawCellAtWithTile(dx, dy, items["tile"])
            #print("tile is " + items["tile"])
        

        #print("after fore")
        #self.universeView.drawCellAt(DEFAULT_UNIV_X/2,DEFAULT_UNIV_X/2)


    def file_changed(self, path):
        self.loadText()

    def setCenter(self, x, y):
        self.centerx = x
        self.centery = y
    def getFromCenter(self, x, y):

        return x-self.centerx, y - self.centery

    def getMapCoordinates(self, x, y):
        xcoord, ycoord = self.getFromCenter(x,y)

        return xcoord + DEFAULT_UNIV_X/2, ycoord + DEFAULT_UNIV_Y/2

if __name__ == '__main__':

    try:
        app = QApplication(sys.argv)
        dfview = DwarfView()

        dfview.show()
       
        sys.exit(app.exec_())
    except Exception as e:
        print("exception ")
        print(e)

        sys.exit(0)