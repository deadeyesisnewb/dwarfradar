
local args = {...}
--local jsonFile = io.open(".\\bmods\\radarinfo.txt", "w")

--local olddfprint = dfhackprint
--local olddfprintln = dfhackprintln
--local mat = require('tile-material')







local function prepPos(x, y, z)
    if x ~= nil and y == nil and z == nil then
        if type(x) ~= "table" or type(x.x) ~= "number" or type(x.y) ~= "number" or type(x.z) ~= "number" or x.x == -30000 then
            error "Invalid coordinate argument(s)."
        end
        return x
    else
        if type(x) ~= "number" or type(y) ~= "number" or type(z) ~= "number" or x == -30000 then
            error "Invalid coordinate argument(s)."
        end
        return {x = x, y = y, z = z}
    end
end

-- Internal
local function fixedMat(id)
    local mat = dfhack.matinfo.find(id)
    return function(x, y, z)
        return mat
    end
end

-- BasicMats is a matspec table to pass to GetTileMatSpec or GetTileTypeMat. This particular
-- matspec table covers the common case of returning plant materials for plant tiles and other
-- materials for the remaining tiles.
BasicMats = {
    [df.tiletype_material.AIR] = nil, -- Empty
    [df.tiletype_material.SOIL] = GetLayerMat,
    [df.tiletype_material.STONE] = GetLayerMat,
    [df.tiletype_material.FEATURE] = GetFeatureMat,
    [df.tiletype_material.LAVA_STONE] = GetLavaStone,
    [df.tiletype_material.MINERAL] = GetVeinMat,
    [df.tiletype_material.FROZEN_LIQUID] = fixedMat("WATER:NONE"),
    [df.tiletype_material.CONSTRUCTION] = GetConstructionMat,
    [df.tiletype_material.GRASS_LIGHT] = GetGrassMat,
    [df.tiletype_material.GRASS_DARK] = GetGrassMat,
    [df.tiletype_material.GRASS_DRY] = GetGrassMat,
    [df.tiletype_material.GRASS_DEAD] = GetGrassMat,
    [df.tiletype_material.PLANT] = GetShrubMat,
    [df.tiletype_material.HFS] = nil, -- Eerie Glowing Pit
    [df.tiletype_material.CAMPFIRE] = GetLayerMat,
    [df.tiletype_material.FIRE] = GetLayerMat,
    [df.tiletype_material.ASHES] = GetLayerMat,
    [df.tiletype_material.MAGMA] = nil, -- SMR
    [df.tiletype_material.DRIFTWOOD] = GetLayerMat,
    [df.tiletype_material.POOL] = GetLayerMat,
    [df.tiletype_material.BROOK] = GetLayerMat,
    [df.tiletype_material.ROOT] = GetLayerMat,
    [df.tiletype_material.TREE] = GetTreeMat,
    [df.tiletype_material.MUSHROOM] = GetTreeMat,
    [df.tiletype_material.UNDERWORLD_GATE] = nil, -- I guess this is for the gates found in vaults?
}

-- NoPlantMats is a matspec table to pass to GetTileMatSpec or GetTileTypeMat. This particular
-- matspec table will ignore plants, returning layer materials (or nil for trees) instead.
NoPlantMats = {
    [df.tiletype_material.SOIL] = GetLayerMat,
    [df.tiletype_material.STONE] = GetLayerMat,
    [df.tiletype_material.FEATURE] = GetFeatureMat,
    [df.tiletype_material.LAVA_STONE] = GetLavaStone,
    [df.tiletype_material.MINERAL] = GetVeinMat,
    [df.tiletype_material.FROZEN_LIQUID] = fixedMat("WATER:NONE"),
    [df.tiletype_material.CONSTRUCTION] = GetConstructionMat,
    [df.tiletype_material.GRASS_LIGHT] = GetLayerMat,
    [df.tiletype_material.GRASS_DARK] = GetLayerMat,
    [df.tiletype_material.GRASS_DRY] = GetLayerMat,
    [df.tiletype_material.GRASS_DEAD] = GetLayerMat,
    [df.tiletype_material.PLANT] = GetLayerMat,
    [df.tiletype_material.CAMPFIRE] = GetLayerMat,
    [df.tiletype_material.FIRE] = GetLayerMat,
    [df.tiletype_material.ASHES] = GetLayerMat,
    [df.tiletype_material.DRIFTWOOD] = GetLayerMat,
    [df.tiletype_material.POOL] = GetLayerMat,
    [df.tiletype_material.BROOK] = GetLayerMat,
    [df.tiletype_material.ROOT] = GetLayerMat,
}

-- OnlyPlantMats is a matspec table to pass to GetTileMatSpec or GetTileTypeMat. This particular
-- matspec table will return nil for any non-plant tile. Plant tiles return the plant material.
OnlyPlantMats = {
    [df.tiletype_material.GRASS_LIGHT] = GetGrassMat,
    [df.tiletype_material.GRASS_DARK] = GetGrassMat,
    [df.tiletype_material.GRASS_DRY] = GetGrassMat,
    [df.tiletype_material.GRASS_DEAD] = GetGrassMat,
    [df.tiletype_material.PLANT] = GetShrubMat,
    [df.tiletype_material.TREE] = GetTreeMat,
    [df.tiletype_material.MUSHROOM] = GetTreeMat,
}

-- GetLayerMat returns the layer material for the given tile.
-- AFAIK this will never return nil.
function GetLayerMat(x, y, z)

    local pos = prepPos(x, y, z)


    local region_info = dfhack.maps.getRegionBiome(dfhack.maps.getTileBiomeRgn(pos))
    local map_block = dfhack.maps.ensureTileBlock(pos)

    local biome = df.world_geo_biome.find(region_info.geo_index)

    local layer_index = map_block.designation[pos.x%16][pos.y%16].geolayer_index
    local layer_mat_index = biome.layers[layer_index].mat_index

    return dfhack.matinfo.decode(0, layer_mat_index)
end

-- GetLavaStone returns the biome lava stone material (generally obsidian).
function GetLavaStone(x, y, z)
    local pos = prepPos(x, y, z)

    local regions = df.global.world.world_data.region_details

    local rx, ry = dfhack.maps.getTileBiomeRgn(pos)

    for _, region in ipairs(regions) do
        if region.pos.x == rx and region.pos.y == ry then
            return dfhack.matinfo.decode(0, region.lava_stone)
        end
    end
    return nil
end

-- GetVeinMat returns the vein material of the given tile or nil if the tile has no veins.
-- Multiple veins in one tile should be handled properly (smallest vein type, last in the list wins,
-- which seems to be the rule DF uses).
function GetVeinMat(x, y, z)
    local pos = prepPos(x, y, z)

    local map_block = dfhack.maps.ensureTileBlock(pos)

    local events = {}
    for _, event in ipairs(map_block.block_events) do
        if getmetatable(event) == "block_square_event_mineralst" then
            if dfhack.maps.getTileAssignment(event.tile_bitmask, pos.x, pos.y) then
                table.insert(events, event)
            end
        end
    end

    if #events == 0 then
        return nil
    end

    local event_priority = function(event)
        if event.flags.cluster then
            return 1
        elseif event.flags.vein then
            return 2
        elseif event.flags.cluster_small then
            return 3
        elseif event.flags.cluster_one then
            return 4
        else
            return 5
        end
    end

    local priority = events[1]
    for _, event in ipairs(events) do
        if event_priority(event) >= event_priority(priority) then
            priority = event
        end
    end

    return dfhack.matinfo.decode(0, priority.inorganic_mat)
end

-- GetConstructionMat returns the material of the construction at the given tile or nil if the tile
-- has no construction.
function GetConstructionMat(x, y, z)
    local pos = prepPos(x, y, z)

    for _, construction in ipairs(df.global.world.constructions) do
        if construction.pos.x == pos.x and construction.pos.y == pos.y and construction.pos.z == pos.z then
            return dfhack.matinfo.decode(construction)
        end
    end
    return nil
end

-- GetConstructOriginalTileMat returns the material of the tile under the construction at the given
-- tile or nil if the tile has no construction.
function GetConstructOriginalTileMat(x, y, z)
    local pos = prepPos(x, y, z)

    for _, construction in ipairs(df.global.world.constructions) do
        if construction.pos.x == pos.x and construction.pos.y == pos.y and construction.pos.z == pos.z then
            return GetTileTypeMat(construction.original_tile, BasicMats, pos)
        end
    end
    return nil
end

-- GetTreeMat returns the material of the tree at the given tile or nil if the tile does not have a
-- tree or giant mushroom.
-- Currently roots are ignored.
function GetTreeMat(x, y, z)
    local pos = prepPos(x, y, z)

    local function coordInTree(pos, tree)
        local x1 = tree.pos.x - math.floor(tree.tree_info.dim_x / 2)
        local x2 = tree.pos.x + math.floor(tree.tree_info.dim_x / 2)
        local y1 = tree.pos.y - math.floor(tree.tree_info.dim_y / 2)
        local y2 = tree.pos.y + math.floor(tree.tree_info.dim_y / 2)
        local z1 = tree.pos.z
        local z2 = tree.pos.z + tree.tree_info.body_height

        if not ((pos.x >= x1 and pos.x <= x2) and (pos.y >= y1 and pos.y <= y2) and (pos.z >= z1 and pos.z <= z2)) then
            return false
        end

        return not tree.tree_info.body[pos.z - tree.pos.z]:_displace((pos.y - y1) * tree.tree_info.dim_x + (pos.x - x1)).blocked
    end

    for _, tree in ipairs(df.global.world.plants.all) do
        if tree.tree_info ~= nil then
            if coordInTree(pos, tree) then
                return dfhack.matinfo.decode(419, tree.material)
            end
        end
    end
    return nil
end

-- GetShrubMat returns the material of the shrub at the given tile or nil if the tile does not
-- contain a shrub or sapling.
function GetShrubMat(x, y, z)
    local pos = prepPos(x, y, z)

    for _, shrub in ipairs(df.global.world.plants.all) do
        if shrub.tree_info == nil then
            if shrub.pos.x == pos.x and shrub.pos.y == pos.y and shrub.pos.z == pos.z then
                return dfhack.matinfo.decode(419, shrub.material)
            end
        end
    end
    return nil
end

-- GetGrassMat returns the material of the grass at the given tile or nil if the tile is not
-- covered in grass.
function GetGrassMat(x, y, z)
    local pos = prepPos(x, y, z)

    local map_block = dfhack.maps.ensureTileBlock(pos)

    for _, event in ipairs(map_block.block_events) do
        if getmetatable(event) == "block_square_event_grassst" then
            local amount = event.amount[pos.x%16][pos.y%16]
            printall(event)
            if amount > 0 then
                return df.plant_raw.find(event.plant_index)
            end
        end
    end
    return nil
end

-- GetFeatureMat returns the material of the feature (adamantine tube, underworld surface, etc) at
-- the given tile or nil if the tile is not made of a feature stone.
function GetFeatureMat(x, y, z)
    local pos = prepPos(x, y, z)

    local map_block = dfhack.maps.ensureTileBlock(pos)

    if df.tiletype.attrs[map_block.tiletype[pos.x%16][pos.y%16]].material ~= df.tiletype_material.FEATURE then
        return nil
    end

    if map_block.designation[pos.x%16][pos.y%16].feature_local then
        -- adamantine tube, etc
        for id, idx in ipairs(df.global.world.features.feature_local_idx) do
            if idx == map_block.local_feature then
                return dfhack.matinfo.decode(df.global.world.features.map_features[id])
            end
        end
    elseif map_block.designation[pos.x%16][pos.y%16].feature_global then
        -- cavern, magma sea, underworld, etc
        for id, idx in ipairs(df.global.world.features.feature_global_idx) do
            if idx == map_block.global_feature then
                return dfhack.matinfo.decode(df.global.world.features.map_features[id])
            end
        end
    end

    return nil
end

-- GetTileMat will return the material of the specified tile as determined by its tile type and the
-- world geology data, etc.
-- The returned material should exactly match the material reported by DF except in cases where is
-- is impossible to get a material.
-- This is equivalent to calling GetTileMatSpec with the BasicMats matspec table.
function GetTileMat(x, y, z)
    return GetTileMatSpec(BasicMats, x, y, z)
end

-- GetTileMatSpec is exactly like GetTileMat except you may specify an explicit matspec table.
--
-- "matspec" tables are simply tables with tiletype material classes as keys and functions
-- taking a coordinate table and returning a material as values. These tables are used to
-- determine how a specific material for a given tiletype material classification is determined.
-- Any tiletype material class that is unset (left nil) in a matspec table will result in tiles
-- of that type returning nil for their material.
function GetTileMatSpec(matspec, x, y, z)

    local pos = prepPos(x, y, z)


    local typ = dfhack.maps.getTileType(pos)
    if typ == nil then
        return nil
    end

    return GetTileTypeMat(typ, matspec, pos)
end

-- GetTileTypeMat returns the material of the given tile assuming it is the given tiletype.
--
-- Use this function when you want to check to see what material a given tile would be if it
-- was a specific tiletype. For example you can check to see if the tile used to be part of
-- a mineral vein or similar. Note that you can do the same basic thing by calling the individual
-- material finders directly, but this is sometimes simpler.
--
-- Unless the tile could be the given type this function will probably return nil.
function GetTileTypeMat(typ, matspec, x, y, z)
    local pos = prepPos(x, y, z)

    local type_mat = df.tiletype.attrs[typ].material

    local mat_getter = matspec[type_mat]

    if mat_getter == nil then
        return nil
    end
    return mat_getter(pos)
end







--------------------------------------------------


local JSONData = ""

local function printJSON(...)
    --print(...)
    --io.output(jsonFile)
    --io.write(...)
    JSONData = JSONData .. ...
end

local testunit = nil

local z = 0
local left = 0  
local right = 0
local top = 0
local bottom = 0
local itemCount = 0
local tilestyle = '0'
local checkid = 0


if args[1] == "UNIT" then

    if args[2] == nil then
        print("You must specify a unit id to follow")
        return
    end

    testunit = df.unit.find(tonumber(args[2]))
    local checkid = testunit.id
    if testunit == nil then


        _G.RadarLocation = {x=0, y=0, z=0}

        print("Nothing found in radar")

        printJSON("{")
        printJSON("\"creatures\":[")
        
        printJSON("{")
        
        printJSON("\"name\":\"Unknown\"")
        printJSON(",\"xcoord\": 0")
        printJSON(",\"ycoord\": 0")
        printJSON(",\"zcoord\": 0")
        printJSON(",\"descript\":\"unit no longer exists\"")
        printJSON(",\"tile\":\"0\"")
        printJSON(",\"fgcolor\":\"0\"")
        printJSON(",\"bgcolor\":\"0\"")
        printJSON(",\"alpha\":\"0\"")
        printJSON(",\"hascolor\":\"false\"")

        
        printJSON("}")
        printJSON("], \"tiles\":[")


        goto show
    end

elseif args[1] == "LOCATION" then


    
    if args[2] == nil or args[3] == nil or args[4] == nil then
        print("You must specify x, y and z coordinates")
        return
    end

    
    _G.RadarLocation = {x=args[2], y=args[3], z=args[4]}


    printJSON("{")
    printJSON("\"creatures\":[")
    
    printJSON("{")
    
    printJSON("\"name\":\"location \"")
    printJSON(",\"xcoord\": "..args[2])
    printJSON(",\"ycoord\": "..args[3])
    printJSON(",\"zcoord\": "..args[4])
    printJSON(",\"descript\":\"area at the tracked location\"")
    printJSON(",\"tile\":\"255\"")
    printJSON(",\"fgcolor\":\"0\"")
    printJSON(",\"bgcolor\":\"0\"")
    printJSON(",\"alpha\":\"0\"")
    printJSON(",\"hascolor\":\"true\"")

    
    printJSON("}")

    z = tonumber(args[4])
    left = tonumber(args[2]) - 10  
    right = tonumber(args[2]) + 10
    top = tonumber(args[3]) + 10
    bottom = tonumber(args[3]) - 10


    goto critters

else
    print("You must either follow a unit or specific location")
    return

end







--for _,unit in ipairs(df.global.world.units.all) do
    --print (unit.id)
  -- if(unit.id == tonumber(args[1])) then
    --    testunit = unit
      --  break
  -- end
--end
--print(testunit.id)
--print(testunit.name.first_name)
--printall(testunit.pos)

_G.RadarLocation = {x=testunit.pos.x, y=testunit.pos.y, z=testunit.pos.z}


printJSON("{")
printJSON("\"creatures\":[")

printJSON("{")

printJSON("\"name\":\"".. testunit.name.first_name .. "\"")
printJSON(",\"xcoord\":".. testunit.pos.x)
printJSON(",\"ycoord\":".. testunit.pos.y)
printJSON(",\"zcoord\":".. testunit.pos.z)
printJSON(",\"descript\":\""..df.global.world.raws.creatures.all[testunit.race].name[0] .. "\"")
printJSON(",\"tile\":\""..df.global.world.raws.creatures.all[testunit.race].creature_tile.."\"")
printJSON(",\"fgcolor\":\""..df.global.world.raws.creatures.all[testunit.race].color[0].."\"")
printJSON(",\"bgcolor\":\""..df.global.world.raws.creatures.all[testunit.race].color[1].."\"")
printJSON(",\"alpha\":\""..df.global.world.raws.creatures.all[testunit.race].color[2].."\"")
printJSON(",\"hascolor\":\"true\"")
z = testunit.pos.z
left = testunit.pos.x - 10  
right = testunit.pos.x + 10
top = testunit.pos.y + 10
bottom = testunit.pos.y - 10

printJSON("}")

::critters::




for _,unit in ipairs(df.global.world.units.all) do
    if(unit.pos.z == z and unit.id ~= checkid) then
        --print(unit.name.first_name .. " is on the level " .. unit.pos.z)
        if(unit.pos.x > left and unit.pos.x < right 
        and unit.pos.y < top and unit.pos.y > bottom ) then
            --print(unit.name.first_name .. "is nearby ")
            --print(unit.pos.y .. " " ..unit.pos.x)
            --print(testunit.pos.y .. " " .. testunit.pos.x)

        
            printJSON(",{")
            printJSON("\"name\":\"".. unit.name.first_name .. "\"")
            printJSON(",\"xcoord\":".. unit.pos.x)
            printJSON(",\"ycoord\":".. unit.pos.y)
            --printJSON(",\"zcoord\":".. unit.pos.z)

            printJSON(",\"descript\":\""..df.global.world.raws.creatures.all[unit.race].name[0].. "\"")
            printJSON(",\"tile\":\""..df.global.world.raws.creatures.all[unit.race].creature_tile.."\"")
            printJSON(",\"fgcolor\":\""..df.global.world.raws.creatures.all[unit.race].color[0].."\"")
            printJSON(",\"bgcolor\":\""..df.global.world.raws.creatures.all[unit.race].color[1].."\"")
            printJSON(",\"alpha\":\""..df.global.world.raws.creatures.all[unit.race].color[2].."\"")
            printJSON(",\"hascolor\":\"true\"")


            printJSON("}")


        end

    end
    
end
printJSON("]")




--for _,block in ipairs(df.global.world.map.map_blocks) do
  --  if(block.map_pos.z == testunit.pos.z) then

    --    if(block.map_pos.x > left and block.map_pos.x < right 
      --  and block.map_pos.y < top and block.map_pos.y > bottom ) then
        --    --printall(block)
        --end

--    end
--end
printJSON(", \"tiles\":[")


for i=left,right do
    for j=bottom, top do

        --if pcall(function () test = df.tiletype.attrs[dfhack.maps.getTileType(i,j,z)] end) and dfhack.maps.isTileVisible(i,j,z) then

        if pcall(function () test = df.tiletype.attrs[dfhack.maps.getTileType(i,j,z)] end)  then
                item = df.tiletype.attrs[dfhack.maps.getTileType(i,j,z)]
            --printall(item)

            if itemCount ~= 0 then
                printJSON(",")
            end
            printJSON("{")
            printJSON("\"name\":\"".. item.caption .. "\"")
            printJSON(",\"xcoord\":".. i)
            printJSON(",\"ycoord\":".. j)
            --printJSON(",\"zcoord\":".. z)
            itemCount = itemCount + 1
            tilestyle = '255'
            local fg = '0'
            local bg = '0'
            local ag = '0'
            local hascolor = 'false'
            

            if item.shape == 0 then
                tilestyle='250'
                fg = GetLayerMat(i,j,z).material.basic_color[0]
                bg = 0
                ag = GetLayerMat(i,j,z).material.basic_color[1]
                hascolor = 'true'
                --tilestyle=GetLayerMat(i,j,z).material.tile

                local type = dfhack.maps.getTileType(prepPos(i, j, z-1))
                materialtype = df.tiletype.attrs[type].material
                if materialtype == df.tiletype_material.POOL or materialtype == df.tiletype_material.BROOK then
                    tilestyle='247'
  
                    fg = 1
                    bg = 0
                    ag = 1
                    hascolor = 'true'
                end

            elseif item.shape == 1 then

                if item.material == df.tiletype_material.MAGMA then
                    tilestyle='247'
  
                    fg = 4
                    bg = 0
                    ag = 0
                    hascolor = 'true'

                end
       
     
            elseif item.shape == 2 then

                tilestyle='236'
                fg = matxxx.material.tile_color[0]
                bg = matxxx.material.tile_color[1]
                ag = matxxx.material.tile_color[2]
                hascolor = 'true'           

            elseif item.shape == 3 then

                tilestyle='46'
                if matxxx ~= nil then
                    fg = matxxx.material.tile_color[0]
                    bg = matxxx.material.tile_color[1]
                    ag = matxxx.material.tile_color[2]
                    hascolor = 'true'           
                end


            elseif item.shape == 4 then
                --printall(item)
                matxxx = GetTileMat(i,j,z)
                if matxxx == nil then
                    return
                end

                local type = dfhack.maps.getTileType(prepPos(i, j, z))

                directions = tostring(item.direction):gsub("-", "")
                
                fg = matxxx.material.tile_color[0]
                ag = matxxx.material.tile_color[2]
                hascolor = 'true'

                if directions == "" then
                    bg = matxxx.material.tile_color[1]
                else
                    bg = 0
                end

                tilestyle = matxxx.material.tile
                materialtype = df.tiletype.attrs[type].material

                if materialtype == df.tiletype_material.TREE then
                    tilestyle = '79'

                    fg = matxxx.material.build_color[0]
                    bg = matxxx.material.build_color[1]
                    ag = matxxx.material.build_color[2]
                    --printall(matxxx.material)

                elseif materialtype == df.tiletype_material.CONSTRUCTION then
                    tilestyle = '79'
                    fg = matxxx.material.build_color[0]

                end

                if directions == 'NESW' then
                    tilestyle='206'
                elseif directions == 'NES' then
                    tilestyle='185'
                elseif directions == 'NEW' or directions == 'SWE' or directions == 'SEW' then
                    tilestyle='203'
                elseif directions == 'NE' then
                    tilestyle='200'        
                elseif directions == 'SSW' then
                    tilestyle='183'     
                elseif directions == 'SSE' then
                    tilestyle='214'                         
                elseif directions == 'SWW' then
                    tilestyle='184'
                elseif directions == 'NSW' then
                    tilestyle='185'       
                elseif directions == 'NSE' then
                    tilestyle='204'                            
                elseif directions == 'NS' then
                    tilestyle='186'     
                elseif directions == 'NW' then
                    tilestyle='188'   
                elseif directions == 'N' then
                    tilestyle='205'        
                elseif directions == 'NEE' then
                    tilestyle='212'   
                elseif directions == 'SEE' then
                    tilestyle='213'                                              
                elseif directions == 'ESW' or directions == 'NWE' then
                    tilestyle='202'      
                elseif directions == 'ES' or directions == 'SE' then
                    tilestyle='201'     
                elseif directions == 'NNW' then
                    tilestyle='189'       
                elseif directions == 'NNE' then
                    tilestyle='211'   
                elseif directions == 'NSEW' or directions == 'NSWE' then
                    tilestyle='206'   
                                                 
                elseif directions == 'NWW' then
                    tilestyle='190'      
                elseif directions == 'NSE' then
                    tilestyle='204'
                elseif directions == 'EW' then
                    tilestyle='205'
                elseif directions == 'WE' then
                    tilestyle='205'           
                elseif directions == 'E' then
                    tilestyle='186'                                               
                elseif directions == 'SW' then
                    tilestyle='187'  
                elseif directions == 'S' then
                    tilestyle='205'  
                elseif directions == 'W' then
                    tilestyle='186'    
                else
                    if directions ~= "" then
                        print(directions)
                    end                  
                end

            elseif item.shape == 6 then
                tilestyle='60'

                fg = GetLayerMat(i,j,z).material.tile_color[0]
                bg = 0
                ag = GetLayerMat(i,j,z).material.tile_color[2]
                hascolor = 'true'
            elseif item.shape == 7 then
   
                tilestyle='62'
                fg = GetLayerMat(i,j,z).material.tile_color[0]
                bg = 0
                ag = GetLayerMat(i,j,z).material.tile_color[2]
                hascolor = 'true'
            elseif item.shape == 8 then

                tilestyle='88'
                fg = GetLayerMat(i,j,z).material.tile_color[0]
                bg = 0
                ag = GetLayerMat(i,j,z).material.tile_color[2]
                hascolor = 'true'
            elseif item.shape == 9 then
                tilestyle='30'

                fg = GetLayerMat(i,j,z).material.tile_color[0]
                bg = 0
                ag = GetLayerMat(i,j,z).material.tile_color[2]
                hascolor = 'true'

            elseif item.shape == 10 or item.shape == 11 then

                tilestyle='31'
                fg = GetLayerMat(i,j,z).material.tile_color[0]
                bg = 0
                ag = GetLayerMat(i,j,z).material.tile_color[2]
                hascolor = 'true'

                local type = dfhack.maps.getTileType(prepPos(i, j, z-1))
                materialtype = df.tiletype.attrs[type].material
                if materialtype == df.tiletype_material.POOL or materialtype == df.tiletype_material.BROOK then 
                    tilestyle='247'
 
                    fg = 1
                    bg = 0
                    ag = 1
                end
            
            elseif item.shape == 15 then

                tilestyle='59'
                fg = matxxx.material.tile_color[0]
                bg = matxxx.material.tile_color[1]
                ag = matxxx.material.tile_color[2]
                hascolor = 'true'           



            elseif item.shape == 16 then

                tilestyle='59'
                fg = GetLayerMat(i,j,z).material.tile_color[0]
                bg = 0
                ag = GetLayerMat(i,j,z).material.tile_color[2]
                hascolor = 'true'
            
            elseif item.shape == 17 then

                tilestyle='34'
                fg = GetLayerMat(i,j,z).material.tile_color[0]
                bg = 0
                ag = GetLayerMat(i,j,z).material.tile_color[2]
                hascolor = 'true'
            
            elseif item.shape == 18 then

                tilestyle='167'
                fg = GetLayerMat(i,j,z).material.tile_color[0]
                bg = 0
                ag = GetLayerMat(i,j,z).material.tile_color[2]
                hascolor = 'true'
            
            else
            --print(item.shape)

             --print("caption: " .. item.caption)
             --print("shape: ".. item.shape)
             --print("material: ".. item.material)
             --print("variant: ".. item.variant)
             --print("special: ".. item.special)
            -- print("direction: ".. item.direction)
                tilestyle='255'
            --print("------")
            end


            --print(i .. ":" ..j .. " ZZZZZZZZZzzzz")
            --print(df.tiletype._attr_entry_type)
            printJSON(",\"tile\":\""..tilestyle.."\"")
            printJSON(",\"fgcolor\":\""..fg.."\"")
            printJSON(",\"bgcolor\":\""..bg.."\"")
            printJSON(",\"alpha\":\""..ag.."\"")
            printJSON(",\"hascolor\":\""..hascolor.."\"")
            printJSON("}")

        end
    end

--io.close(jsonFile)
end

::show::

printJSON("]")

printJSON("}")

socket = require("plugins.luasocket")
host, port = "localhost", 9999


conx = socket.tcp:connect(host, port);

msg = "this is at atest"

conx:send(JSONData)
conx:close()
