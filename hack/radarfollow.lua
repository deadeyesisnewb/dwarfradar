
local args = {...}




if args[1] == "UNIT" then

    if args[2] == nil then
        print("You must specify a unit id to follow")
        return
    end
 
    local testunit = false
    for _,unit in ipairs(df.global.world.units.all) do
        --print (unit.id)
       if(unit.id == tonumber(args[2])) then
            testunit = true
            break
       end
    end
    
    if testunit == false then
        print("There is no unit with the id " .. args[1])
        return
    end

    dfhack.run_command("repeat -name radarjob -cancel")
    print("Starting tracker")
        
    dfhack.run_command("repeat -name radarjob -time 10 -timeUnits ticks -command [ radar \"UNIT\" "..tonumber(args[2]).." ]")

    dfhack.run_command("imageviewer ".. tonumber(args[2]) .. " Radar-Following " .. 9997)


elseif args[1] == "LOCATION" then
    
    if args[2] == nil or args[3] == nil or args[4] == nil then
        print("You must specify x, y and z coordinates")
        return
    end

    dfhack.run_command("repeat -name radarjob -cancel")
    print("Starting tracker")
        
    dfhack.run_command("repeat -name radarjob -time 10 -timeUnits ticks -command [ radar \"LOCATION\" "..tonumber(args[2]).." "..tonumber(args[3]).." "..tonumber(args[4]).." ]")
    --dfhack.run_command("imageviewer 99999 Radar-Location " .. 9997)
    

else
    print("You must either follow a unit or specific location")
    return

end






--print(testunit.id)
