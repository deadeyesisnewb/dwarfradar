
local args = {...}

printresults = true

if args[1] == "quiet" then
    printresults = false
end


function spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end
  
    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys 
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end
  
    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end



jsonData = "{"

jsonData = jsonData .. "\"crittercount\":"
jsonData = jsonData .. "{"
jsonData = jsonData .. "\"critters\" : ["


count = 0
unitTable = {}

--dorf = {}
for _,unit in ipairs(df.global.world.units.all) do
    if(df.global.world.raws.creatures.all [unit.race].name [0] ~= "dwarf" and unit.flags2.killed == false) then
        --dorf[count] = unit
        unitTable[unit.id] = dfhack.TranslateName(dfhack.units.getVisibleName(unit)) .. " " .. df.global.world.raws.creatures.all [unit.race].name [0] 
    end

end

pcount = 0

if printresults then
    print("-------------------")
end

for id,name in spairs(unitTable, function(t,a,b) return t[b] > t[a] end) do
    if count > 0 then
        jsonData = jsonData .. ","
    end
    jsonData = jsonData .. "\""..  name .. " : " .. tostring(id) .. "\""
    count = count + 1
    if printresults then
        pcount = pcount + 1
        print(name.. " - " .. id )
    end
end
if printresults then
    print(pcount)
end
jsonData = jsonData .. "]"

jsonData = jsonData .. "}"
jsonData = jsonData .. "}"

